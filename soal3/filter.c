#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>
#include <dirent.h>
#include <string.h>
#include <fcntl.h>

#define PATH_SIZE 1024
#define BUF_SIZE 1024

typedef struct PlayerName{
    char fileName[128];
    int rating;
} PlayerName;

PlayerName *parseDirectory(char *directory){
    struct dirent *entry;
    static PlayerName players[16];
    DIR *dir = opendir(directory);

    if (directory == NULL){
        return NULL;
    }

    int i = 0;

    while((entry = readdir(dir)) != NULL){
        char *fileName = entry->d_name, *token;
        if(!(strcmp(fileName, ".") == 0) && !(strcmp(fileName, "..")) == 0){
            strcpy(players[i].fileName, fileName);
            token = strtok(fileName, "_");
            token = strtok(NULL, "_");
            token = strtok(NULL, "_");
            token = strtok(NULL, "_");
            sscanf(token, "%d", &players[i].rating);
            i++;
        }
    }
    strcpy(players[i].fileName, "END");
    players[i].rating = -1;

    closedir(dir);
    return players;
}

void dowload() {
    pid_t pid = fork();
    if (pid == -1) {
        perror("ERROR");
        exit(EXIT_FAILURE);
    }
    if (pid == 0){
        char *args[] = {
            "wget", "-O", "players.zip", "https://drive.google.com/u/0/uc?id=1zEAneJ1-0sOgt13R1gL4i1ONWfKAtwBF&export=download", NULL};
            execvp(args[0], args);
            perror("ERROR");
            exit(EXIT_FAILURE);
        }
    else {
        wait(NULL);
    }
 }

void extract() {
    pid_t pid = fork();
    if (pid == -1) {
        perror("ERROR");
        exit(EXIT_FAILURE);
    }
    if (pid == 0){
        char *args[] = {"unzip", "players.zip",  NULL};
            execvp(args[0], args);
            perror("ERROR");
            exit(EXIT_FAILURE);
        }
    else {
        wait(NULL);
    }
}

void FilterMU() {
    DIR *d;
    struct dirent *dir;
    d = opendir(".");
    if (d) {
        while ((dir = readdir(d)) != NULL){
            if (dir->d_type == DT_DIR && strcmp(dir->d_name, "players") == 0) {
                DIR *d_players = opendir("players");
                if (d_players){
                    struct dirent *dir_players;
                    while ((dir_players = readdir(d_players)) != NULL)
                    {
                        if (dir_players->d_type == DT_REG) {
                            char *fileName = dir_players->d_name;
                            if(strstr(fileName, "ManUtd") == NULL){
                                char filePath[100] = "players/";
                                strcat(filePath, fileName);
                                remove(filePath);
                            }
                        }
                    }
                    closedir(d_players);
                }
            }
        }
        closedir(d);
    }
}

void makeFolder(){
    
    pid_t pid = fork();
    if (pid == -1) {
        perror("ERROR");
        exit(EXIT_FAILURE);
    }
    if (pid == 0){
        char *args1[] = {"mkdir", "players/Gelandang", "players/Kiper", "players/Bek", "players/Penyerang", NULL};
        execvp(args1[0], args1);
        perror("ERROR");
        exit(EXIT_FAILURE);
        }
    else {
        wait(NULL);
    }

}

void categorize(){
    
    DIR *d;
    struct dirent *dir;
    d = opendir(".");
    pid_t pid = fork();

    if(pid == -1){
        perror("ERROR");
        exit(EXIT_FAILURE);
    }

    if(pid == 0){
        if (d) {
            while ((dir = readdir(d)) != NULL){
                if (dir->d_type == DT_DIR && strcmp(dir->d_name, "players") == 0) {
                    DIR *d_players = opendir("players");
                    if (d_players){
                        struct dirent *dir_players;
                        while ((dir_players = readdir(d_players)) != NULL)
                        {
                            if (dir_players->d_type == DT_REG) {
                                char *fileName = dir_players->d_name;
                                if(strstr(fileName, "Gelandang") != NULL && strstr(fileName, "png") != NULL){
                                    char filePath[100] = "players/";
                                    char *file = strcat(filePath, fileName);
                                    char *args[] = {"mv", file, "players/Gelandang", NULL};
                                    pid_t pid = fork();
                                    
                                    if(pid == 0){
                                    execvp("mv", args);
                                    }
                                    
                                    else{
                                    wait(NULL);
                                    }
                                    
                                }
                                else if(strstr(fileName, "Kiper") != NULL && strstr(fileName, "png") != NULL){
                                    char filePath[100] = "players/";
                                    char *file = strcat(filePath, fileName);
                                    char *args[] = {"mv", file, "players/Kiper", NULL};
                                    pid_t pid = fork();
                                    
                                    if(pid == 0){
                                    execvp("mv", args);
                                    }
                                    
                                    else{
                                    wait(NULL);
                                    }
                                    
                                }
                                else if(strstr(fileName, "Bek") != NULL && strstr(fileName, "png") != NULL){
                                    char filePath[100] = "players/";
                                    char *file = strcat(filePath, fileName);
                                    char *args[] = {"mv", file, "players/Bek", NULL};
                                    pid_t pid = fork();
                                    
                                    if(pid == 0){
                                    execvp("mv", args);
                                    }
                                    
                                    else{
                                    wait(NULL);
                                    }
                                    
                                }
                                else if(strstr(fileName, "Penyerang") != NULL && strstr(fileName, "png") != NULL){
                                    char filePath[100] = "players/";
                                    char *file = strcat(filePath, fileName);
                                    char *args[] = {"mv", file, "players/Penyerang", NULL};
                                    pid_t pid = fork();
                                    
                                    if(pid == 0){
                                    execvp("mv", args);
                                    }
                                    
                                    else{
                                    wait(NULL);
                                    }
                                    
                                }
                            }
                        }
                        closedir(d_players);
                    }
                }
            }
            closedir(d);
        }
    
}
    

}

int getArrSize(PlayerName *arr) {
    int i = 0;
    while (!(strcmp(arr[i].fileName, "END") == 0) && (arr[i].rating != -1)) {
        i++;
    }
    return i;
}

void bubbleSort(PlayerName *arr, int n) {
    int swapped;
    for (int i = 0; i < n - 1; i++) {
        swapped = 0;

        for (int j = 0; j < n - i - 1; j++) {
           
            if (arr[j].rating < arr[j + 1].rating) {
           
                PlayerName tmp = arr[j];
                arr[j] = arr[j + 1];
                arr[j + 1] = tmp;
            }
        }
        if (swapped) break;
    }
}

void buatTim(int bek, int gelandang, int penyerang) {

    char outputFile[32] = "";
    snprintf(outputFile + strlen(outputFile), 32 - strlen(outputFile),
             "Formasi_%d-%d-%d.txt", bek, gelandang, penyerang);
    FILE *f = fopen(outputFile, "w");

    PlayerName *kiperArr = parseDirectory("players/Kiper");
    bubbleSort(kiperArr, getArrSize(kiperArr));

    fwrite(kiperArr[0].fileName, sizeof(char), strlen(kiperArr[0].fileName), f);
    fwrite("\n", sizeof(char), strlen("\n"), f);

    PlayerName *bekArr = parseDirectory("players/Bek");
    bubbleSort(bekArr, getArrSize(bekArr));

    for (int i = 0; i < bek; i++) {
        fwrite(bekArr[i].fileName, sizeof(char), strlen(bekArr[i].fileName), f);
        fwrite("\n", sizeof(char), strlen("\n"), f);
    }

    PlayerName *gelandangArr = parseDirectory("players/Gelandang");
    bubbleSort(gelandangArr, getArrSize(gelandangArr));

    for (int i = 0; i < gelandang; i++) {
        fwrite(gelandangArr[i].fileName, sizeof(char),
               strlen(gelandangArr[i].fileName), f);
        fwrite("\n", sizeof(char), strlen("\n"), f);
    }

    PlayerName *penyerangArr = parseDirectory("players/Penyerang");
    bubbleSort(penyerangArr, getArrSize(penyerangArr));

    for (int i = 0; i < penyerang; i++) {
        fwrite(penyerangArr[i].fileName, sizeof(char),
               strlen(penyerangArr[i].fileName), f);
        fwrite("\n", sizeof(char), strlen("\n"), f);
    }

    fclose(f);
}

int main(){
    dowload();
    extract();
    unlink("players.zip");
    FilterMU();
    makeFolder();
    categorize();
    buatTim(4,3,3);
    return 0;
    
}
