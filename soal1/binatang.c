#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <string.h>
#include <wait.h>
#include <limits.h>
#include <time.h>
#include <dirent.h>
#include <sys/types.h>
#include <fcntl.h>
#include <errno.h>
#include <syslog.h>
#include <dirent.h>

#define _GNU_SOURCE
#define DARAT_DIR "/home/ubuntu/Documents/prak-sisop/modul2/HewanDarat"
#define AMPHIBI_DIR "/home/ubuntu/Documents/prak-sisop/modul2/HewanAmphibi"
#define AIR_DIR "/home/ubuntu/Documents/prak-sisop/modul2/HewanAir"
#define BASE_PATH "/home/ubuntu/Documents/prak-sisop/modul2/animals"

void zip_newdir(){
        pid_t waiter, HewDar, HewAm, HewAir;
        HewDar = fork();
        HewAm = fork();
        HewAir = fork();

        if (HewDar == 0 && HewAm > 0 && HewAir > 0) {
                char *args[] = {"/bin/zip", "zip", "-r", "HewanDarat.zip", "HewanDarat", NULL};
                printf("HewanDarat.zip is created successfully!\n");

                if(execv(args[0],args+1) == -1) {
                        perror("zip failed!");
                        exit(EXIT_FAILURE);
                }

        }
        else if (HewDar > 0 && HewAm == 0 && HewAir > 0) {
                char *args[] = {"/bin/zip", "zip", "-r", "HewanAmphibi.zip", "HewanAmphibi", NULL};
                printf("HewanAmphibi.zip is created successfully!\n");

                if(execv(args[0],args+1) == -1) {
                        perror("zip failed!");
                        exit(EXIT_FAILURE);
                }

        }
        else if (HewDar > 0 && HewAm > 0 && HewAir == 0) {
                char *args[] = {"/bin/zip", "zip", "-r", "HewanAir.zip", "HewanAir", NULL};
                printf("HewanAir.zip is created successfully!\n");

                if(execv(args[0],args+1) == -1) {
                        perror("zip failed!");
                        exit(EXIT_FAILURE);
                }

        }
        else if (HewDar > 0 && HewAm > 0 && HewAir > 0) {
        waiter = wait(NULL);
        waiter = wait(NULL);
        }
}

void filter_file(){
    pid_t dar = 0, amp = 0, air = 0;
    int status;
    char source[1000], destination[1000];
    struct dirent *dp;
    DIR *dir = opendir(BASE_PATH);

    if (!dir){
        perror("Error opening directory");
        return;
    }

    while ((dp = readdir(dir)) != NULL){
        if (strstr(dp->d_name, "_") == NULL){
            continue;
        }
        sprintf(source, "%s/%s", BASE_PATH, dp->d_name);
        printf("Moving the %s file\n", dp->d_name);

        if (strstr(dp->d_name, "_darat") != NULL){
            sprintf(destination, DARAT_DIR "/%s", dp->d_name);
            dar = fork();
            if (dar == 0){
                execlp("mv", "mv", source, destination, NULL);
                perror("Error occurred while moving darat file");
                exit(EXIT_FAILURE);
            }
        }
        if (strstr(dp->d_name, "_amphibi") != NULL){
            sprintf(destination, AMPHIBI_DIR "/%s", dp->d_name);
            amp = fork();
            if (amp == 0){
                execlp("mv", "mv", source, destination, NULL);
                perror("Error occurred while moving amphibi file");
                exit(EXIT_FAILURE);
            }
        }
        if (strstr(dp->d_name, "_air") != NULL){
            sprintf(destination, AIR_DIR "/%s", dp->d_name);
            air = fork();
            if (air == 0){
                execlp("mv", "mv", source, destination, NULL);
                perror("Error occurred while moving air file");
                exit(EXIT_FAILURE);
            }
        }
    }

    closedir(dir);

    while ((waitpid(dar, &status, 0) > 0) || (waitpid(amp, &status, 0) > 0) || (waitpid(air, &status, 0) > 0));

    printf("All files have been moved successfully\n");

    zip_newdir();
}


void make_directory(){
	pid_t waiter, dirname1, dirname2, dirname3;
	dirname1 = fork();
	dirname2= fork();
	dirname3= fork();
        if (dirname1 > 0 && dirname2 > 0 && dirname3 > 0){
                waiter = wait(NULL);
                waiter = wait(NULL);
		filter_file();
        }

        else if (dirname1 == 0 && dirname2 == 0 && dirname3 == 0){
                char *args[5] = {"/bin/mkdir", "mkdir", "-p" , "HewanDarat", NULL};
                printf("HewanDarat directory created successfully.\n");

                if(execv(args[0],args+1) == -1){
                perror("making a new directory called HewanDarat is failed!");
                exit(EXIT_FAILURE);
                }
        }

       else if (dirname1 > 0 && dirname2 == 0 && dirname3 > 0){
                char *args[5] = {"/bin/mkdir", "mkdir", "-p" , "HewanAmphibi", NULL};
                printf("HewanAmphibi directory created successfully.\n");

                if(execv(args[0],args+1) == -1){
                perror("making a new directory called HewanAmphibi is failed!");
                exit(EXIT_FAILURE);
                }
        }

        else if (dirname1 > 0 && dirname2 > 0 && dirname3 == 0){
                char *args[5] = {"/bin/mkdir", "mkdir", "-p" , "HewanAir", NULL};
                printf("HewanAir directory created successfully.\n");

                if(execv(args[0],args+1) == -1){
                perror("making a new directory called HewanAir is failed!");
                exit(EXIT_FAILURE);
                }
        }
}

void random_animal() {
    char buf[PATH_MAX];
    DIR* dir;
    struct dirent *dir_ent;
    int number_files = 0;
    struct stat st;

    srand(time(NULL));

    dir = opendir(BASE_PATH);
    if (dir == NULL) {
        fprintf(stderr, "Error: Unable to open the animals directory.\n");
        return;
    }

    while ((dir_ent = readdir(dir)) != NULL) {
        char filepath[PATH_MAX];

        if (dir_ent->d_type == DT_REG) {
            snprintf(filepath, PATH_MAX, "%s/%s", BASE_PATH, dir_ent->d_name);
            if (stat(filepath, &st) == 0 && st.st_mode & S_IRUSR) {
                number_files++;
                if (rand() % number_files == 0) {
                    snprintf(buf, PATH_MAX, "%s", dir_ent->d_name);
                }
            }
        }
    }
    closedir(dir);

    if (number_files == 0) {
        fprintf(stderr, "Error: No readable files in animals directory.\n");
        return;
    }

    printf("The animal that will be looked after by Grape-kun is: %s\n", buf);

    make_directory();
}


void unzip(){
        pid_t unzip_binatang, waiter;
        unzip_binatang = fork();

        if(unzip_binatang == 0){
		printf("Unzipping binatang file\n");

        	char *args[] = {"/bin/unzip", "unzip", "binatang.zip","-d", "animals", NULL};

		if(execv(args[0],args+1) == -1){

                	perror("unzip binatang failed!");
                	exit(EXIT_FAILURE);
        	}
		else { printf("Unzipping success!\n");}
	}

	else if(unzip_binatang > 0){
        	waiter = wait(NULL);
		random_animal();
	}
}

void download_file(char file_name[]){
        pid_t download_binatang, waiter;
        download_binatang = fork();

        if(download_binatang == 0){
                printf("Downloading binatang.zip file\n");

                char *args[] = {"/bin/wget", "wget","--quiet" , "https://drive.google.com/uc?export=download&id=1oDgj5kSiDO0tlyS7-20uz7t20X3atwrq", "-O", file_name, NULL};

                if(execv(args[0],args+1) == -1)
                {
                        perror("Downloading binatang.zip file failed!");
                        exit(EXIT_FAILURE);
                }
		else {printf("Downloading success!\n");}
        }

	else if(download_binatang > 0){
        waiter = wait(NULL);
        unzip();
   }

}

int main(){
        download_file("binatang.zip");

        return 0;
}
