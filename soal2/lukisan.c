#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <sys/stat.h>
#include <unistd.h>
#include <sys/mman.h>
#include <sys/wait.h>
// #include <zip.h>

#define IMAGE_NAME "%Y-%m-%d_%H:%M:%S.jpg"

void create_folder() {
    char foldername[30];
    int status;
    time_t now = time(NULL);
    struct tm *tm = localtime(&now);
    strftime(foldername, sizeof(foldername), "folder_%Y-%m-%d_%H:%M:%S", tm);
    mkdir(foldername, 0700);
    chdir(foldername);
    wait(&status);
}

// void create_zip(){
    
// }

void download_images() {
    int status;
    pid_t pid = fork();
    
    for(int i=0; i<=15; i++){
        time_t timeepoch;
        time_t now = time(NULL);
        time_t current_time;
        struct tm *tm = localtime(&now);
        char url[] = "https://picsum.photos/";
        char file_name[30];
        time(&current_time);
        strftime(file_name, sizeof(file_name), "%Y-%m-%d_%H:%M:%S.jpg", tm);
        timeepoch = time(NULL);
        int timeepochsize = (timeepoch%1000)+50;
        char timeepochstr[60];

        sprintf(timeepochstr, "%d", timeepochsize);

        strcat(url, timeepochstr);
        pid = fork();
        if(pid == 0){
            char *command[]={"wget",url, "-O", file_name, NULL};
            execvp(command[0],command);
            printf("Downloading...");
        }
        sleep(5);
    }
    wait(&status);
}

// void kill_script(){
//     FILE* script file = fopen("killer.c", "w");
//     fprintf(script file, "#include <stdio.h>\n#include <unistd.h>\n#include <signal.h>\n");
//     fprintf(script file, "int main(){\nexecvp(\"pkill\",\"lukisan\", NULL);)\nexecvp(\"rm\",\"killer\", NULL);}");  
//     printf("Script stop");
// }


int main() {

    while(1) {
        pid_t pid = fork();
        if (pid == 0) { 
            create_folder();
            download_images();
            exit(0);
        }
        else if (pid < 0) {
            perror("fork error");
            exit(0);
        }
        else {
            sleep(30); 
        }
    }

    return 0;
}

