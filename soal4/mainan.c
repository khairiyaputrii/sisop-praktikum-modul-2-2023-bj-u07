#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <time.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/wait.h>

// execute the file on path
void call_path(const char *program_path)
{
    pid_t pid = fork();
    // Child procss
    if (pid == 0)
    {
        execl(program_path, program_path, NULL);
        exit(0);
    }
    // Parent 
    else if (pid > 0)
    {
        waitpid(pid, NULL, 0);
    }
}

int main(int argc, char *argv[])
{
    pid_t pid, sid;
    int fd, hh, mm, ss, ret;
    time_t now;
    struct tm time_info;
    const char *program_path;

    // Checking the number of argument
    if (argc != 5)
    {
        fprintf(stderr, "Error, invalid argument: ./mainan hh mm ss program_path\n");
        exit(EXIT_FAILURE);
    }

    // Parsing the "*" and checking the format hour, minute, second
    if (strcmp(argv[1], "*") == 0)
    {
        hh = -1;
    }
    // check hour between 0-23
    else
    {
        ret = sscanf(argv[1], "%d", &hh);
        if (ret != 1 || hh < 0 || hh > 23)
        {
            fprintf(stderr, "Invalid hour: %s\n", argv[1]);
            exit(EXIT_FAILURE);
        }
    }
    if (strcmp(argv[2], "*") == 0)
    {
        mm = -1;
    }
    else
    //check minute
    {
        ret = sscanf(argv[2], "%d", &mm);
        if (ret != 1 || mm < 0 || mm > 59)
        {
            fprintf(stderr, "Invalid minute: %s\n", argv[2]);
            exit(EXIT_FAILURE);
        }
    }
    if (strcmp(argv[3], "*") == 0)
    {
        ss = -1;
    }
    //check second
    else
    {
        ret = sscanf(argv[3], "%d", &ss);
        if (ret != 1 || ss < 0 || ss > 59)
        {
            fprintf(stderr, "Invalid second: %s\n", argv[3]);
            exit(EXIT_FAILURE);
        }
    }

    // taking the path argument
    program_path = argv[4];

    // Forking parent
    pid = fork();
    if (pid < 0)
    {
        exit(EXIT_FAILURE);
    }
    if (pid > 0)
    {
        exit(EXIT_SUCCESS);
    }

    umask(0);

    // SID for child
    sid = setsid();
    if (sid < 0)
    {
        exit(EXIT_FAILURE);
    }

    close(STDIN_FILENO);
    close(STDOUT_FILENO);
    close(STDERR_FILENO);

    //  Write the log in append mode
    FILE *fp = fopen("/home/fadhlan/log.txt", "a");
    if (fp == NULL)
    {
        perror("fopen");
        exit(EXIT_FAILURE);
    }

    while (1)
    {
        // Add current time
        now = time(NULL);
        localtime_r(&now, &time_info);

        // checking the time 
        if ((hh == -1 || hh == time_info.tm_hour) &&
            (mm == -1 || mm == time_info.tm_min) &&
            (ss == -1 || ss == time_info.tm_sec))
        {
            // call function to run the path file
            call_path(program_path);
            // To tell if the file successfully executed
            dprintf(fd, "Program successfully executed at %s", ctime(&now));
        }

        // Sleep the program so that i run every second
        sleep(1);
    }

    fclose(fp);

    return EXIT_SUCCESS;
}     
