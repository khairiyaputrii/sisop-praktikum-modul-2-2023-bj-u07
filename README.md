# sisop-praktikum-modul-2-2023-BJ-U07



## Modul 2 Sisop 2023 U07 Formal Report

Group Members:
1. Khairiya Maisa Putri (5025211192)
2. Fauzan Ahmad Faisal (5025211067)
3. Muhammad Fadhlan Ashila Harashta (5025211068)

# No 1


## Question
>Grape-kun adalah seorang penjaga hewan di kebun binatang, dia mendapatkan tugas dari atasannya untuk melakukan penjagaan pada beberapa hewan-hewan yang ada di kebun binatang sebelum melakukan penjagaan Grape-kun harus mengetahui terlebih dahulu hewan apa aja yang harus dijaga dalam drive kebun binatang tersebut terdapat folder gambar dari hewan apa saja yang harus dijaga oleh Grape-kun. Berikut merupakan link download dari drive kebun binatang tersebut : https://drive.google.com/uc?export=download&id=1oDgj5kSiDO0tlyS7-20uz7t20X3atwrq 
>- Grape-kun harus mendownload file tersebut untuk disimpan pada penyimpanan local komputernya. Dan untuk melakukan melihat file gambar pada folder yang telah didownload Grape-kun harus melakukan unzip pada folder tersebut.
>- Setelah berhasil melakukan unzip Grape-kun melakukan pemilihan secara acak pada file gambar tersebut untuk melakukan shift penjagaan pada hewan tersebut.
>- Karena Grape-kun adalah orang yang perfeksionis Grape-kun ingin membuat direktori untuk memilah file gambar tersebut. Direktori tersebut dengan nama HewanDarat, HewanAmphibi, dan HewanAir. Setelah membuat direktori tersebut Grape-kun harus melakukan filter atau pemindahan file gambar hewan sesuai dengan tempat tinggal nya.
>- Setelah mengetahui hewan apa saja yang harus dijaga Grape-kun melakukan zip kepada direktori yang dia buat sebelumnya agar menghemat penyimpanan.
> >Catatan : 
untuk melakukan zip dan unzip tidak boleh menggunakan system

Source code for No. 1:
```c 

#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <string.h>
#include <wait.h>
#include <limits.h>
#include <time.h>
#include <dirent.h>
#include <sys/types.h>
#include <fcntl.h>
#include <errno.h>
#include <syslog.h>
#include <dirent.h>

#define _GNU_SOURCE
#define DARAT_DIR "/home/ubuntu/Documents/prak-sisop/modul2/HewanDarat"
#define AMPHIBI_DIR "/home/ubuntu/Documents/prak-sisop/modul2/HewanAmphibi"
#define AIR_DIR "/home/ubuntu/Documents/prak-sisop/modul2/HewanAir"
#define BASE_PATH "/home/ubuntu/Documents/prak-sisop/modul2/animals"

void zip_newdir(){
        pid_t waiter, HewDar, HewAm, HewAir;
        HewDar = fork();
        HewAm = fork();
        HewAir = fork();

        if (HewDar == 0 && HewAm > 0 && HewAir > 0) {
                char *args[] = {"/bin/zip", "zip", "-r", "HewanDarat.zip", "HewanDarat", NULL};
                printf("HewanDarat.zip is created successfully!\n");

                if(execv(args[0],args+1) == -1) {
                        perror("zip failed!");
                        exit(EXIT_FAILURE);
                }

        }
        else if (HewDar > 0 && HewAm == 0 && HewAir > 0) {
                char *args[] = {"/bin/zip", "zip", "-r", "HewanAmphibi.zip", "HewanAmphibi", NULL};
                printf("HewanAmphibi.zip is created successfully!\n");

                if(execv(args[0],args+1) == -1) {
                        perror("zip failed!");
                        exit(EXIT_FAILURE);
                }

        }
        else if (HewDar > 0 && HewAm > 0 && HewAir == 0) {
                char *args[] = {"/bin/zip", "zip", "-r", "HewanAir.zip", "HewanAir", NULL};
                printf("HewanAir.zip is created successfully!\n");

                if(execv(args[0],args+1) == -1) {
                        perror("zip failed!");
                        exit(EXIT_FAILURE);
                }

        }
        else if (HewDar > 0 && HewAm > 0 && HewAir > 0) {
        waiter = wait(NULL);
        waiter = wait(NULL);
        }
}

void filter_file(){
    pid_t dar = 0, amp = 0, air = 0;
    int status;
    char source[1000], destination[1000];
    struct dirent *dp;
    DIR *dir = opendir(BASE_PATH);

    if (!dir){
        perror("Error opening directory");
        return;
    }

    while ((dp = readdir(dir)) != NULL){
        if (strstr(dp->d_name, "_") == NULL){
            continue;
        }
        sprintf(source, "%s/%s", BASE_PATH, dp->d_name);
        printf("Moving the %s file\n", dp->d_name);

        if (strstr(dp->d_name, "_darat") != NULL){
            sprintf(destination, DARAT_DIR "/%s", dp->d_name);
            dar = fork();
            if (dar == 0){
                execlp("mv", "mv", source, destination, NULL);
                perror("Error occurred while moving darat file");
                exit(EXIT_FAILURE);
            }
        }
        if (strstr(dp->d_name, "_amphibi") != NULL){
            sprintf(destination, AMPHIBI_DIR "/%s", dp->d_name);
            amp = fork();
            if (amp == 0){
                execlp("mv", "mv", source, destination, NULL);
                perror("Error occurred while moving amphibi file");
                exit(EXIT_FAILURE);
            }
        }
        if (strstr(dp->d_name, "_air") != NULL){
            sprintf(destination, AIR_DIR "/%s", dp->d_name);
            air = fork();
            if (air == 0){
                execlp("mv", "mv", source, destination, NULL);
                perror("Error occurred while moving air file");
                exit(EXIT_FAILURE);
            }
        }
    }

    closedir(dir);

    while ((waitpid(dar, &status, 0) > 0) || (waitpid(amp, &status, 0) > 0) || (waitpid(air, &status, 0) > 0));

    printf("All files have been moved successfully\n");

    zip_newdir();
}


void make_directory(){
	pid_t waiter, dirname1, dirname2, dirname3;
	dirname1 = fork();
	dirname2= fork();
	dirname3= fork();
        if (dirname1 > 0 && dirname2 > 0 && dirname3 > 0){
                waiter = wait(NULL);
                waiter = wait(NULL);
		filter_file();
        }

        else if (dirname1 == 0 && dirname2 == 0 && dirname3 == 0){
                char *args[5] = {"/bin/mkdir", "mkdir", "-p" , "HewanDarat", NULL};
                printf("HewanDarat directory created successfully.\n");

                if(execv(args[0],args+1) == -1){
                perror("making a new directory called HewanDarat is failed!");
                exit(EXIT_FAILURE);
                }
        }

       else if (dirname1 > 0 && dirname2 == 0 && dirname3 > 0){
                char *args[5] = {"/bin/mkdir", "mkdir", "-p" , "HewanAmphibi", NULL};
                printf("HewanAmphibi directory created successfully.\n");

                if(execv(args[0],args+1) == -1){
                perror("making a new directory called HewanAmphibi is failed!");
                exit(EXIT_FAILURE);
                }
        }

        else if (dirname1 > 0 && dirname2 > 0 && dirname3 == 0){
                char *args[5] = {"/bin/mkdir", "mkdir", "-p" , "HewanAir", NULL};
                printf("HewanAir directory created successfully.\n");

                if(execv(args[0],args+1) == -1){
                perror("making a new directory called HewanAir is failed!");
                exit(EXIT_FAILURE);
                }
        }
}

void random_animal() {
    char buf[PATH_MAX];
    DIR* dir;
    struct dirent *dir_ent;
    int number_files = 0;
    struct stat st;

    srand(time(NULL));

    dir = opendir(BASE_PATH);
    if (dir == NULL) {
        fprintf(stderr, "Error: Unable to open the animals directory.\n");
        return;
    }

    while ((dir_ent = readdir(dir)) != NULL) {
        char filepath[PATH_MAX];

        if (dir_ent->d_type == DT_REG) {
            snprintf(filepath, PATH_MAX, "%s/%s", BASE_PATH, dir_ent->d_name);
            if (stat(filepath, &st) == 0 && st.st_mode & S_IRUSR) {
                number_files++;
                if (rand() % number_files == 0) {
                    snprintf(buf, PATH_MAX, "%s", dir_ent->d_name);
                }
            }
        }
    }
    closedir(dir);

    if (number_files == 0) {
        fprintf(stderr, "Error: No readable files in animals directory.\n");
        return;
    }

    printf("The animal that will be looked after by Grape-kun is: %s\n", buf);

    make_directory();
}


void unzip(){
        pid_t unzip_binatang, waiter;
        unzip_binatang = fork();

        if(unzip_binatang == 0){
		printf("Unzipping binatang file\n");

        	char *args[] = {"/bin/unzip", "unzip", "binatang.zip","-d", "animals", NULL};

		if(execv(args[0],args+1) == -1){

                	perror("unzip binatang failed!");
                	exit(EXIT_FAILURE);
        	}
		else { printf("Unzipping success!\n");}
	}

	else if(unzip_binatang > 0){
        	waiter = wait(NULL);
		random_animal();
	}
}

void download_file(char file_name[]){
        pid_t download_binatang, waiter;
        download_binatang = fork();

        if(download_binatang == 0){
                printf("Downloading binatang.zip file\n");

                char *args[] = {"/bin/wget", "wget","--quiet" , "https://drive.google.com/uc?export=download&id=1oDgj5kSiDO0tlyS7-20uz7t20X3atwrq", "-O", file_name, NULL};

                if(execv(args[0],args+1) == -1)
                {
                        perror("Downloading binatang.zip file failed!");
                        exit(EXIT_FAILURE);
                }
		else {printf("Downloading success!\n");}
        }

	else if(download_binatang > 0){
        waiter = wait(NULL);
        unzip();
   }

}

int main(){
        download_file("binatang.zip");

        return 0;
}
```

# Explanation
**First** we make a file called `binatang.c` where we store all the code for No 1

```shell script
$ nano binatang.c
```

And to make it easier for me, i defined 4 path.

```c
#define DARAT_DIR "/home/ubuntu/Documents/prak-sisop/modul2/HewanDarat"
#define AMPHIBI_DIR "/home/ubuntu/Documents/prak-sisop/modul2/HewanAmphibi"
#define AIR_DIR "/home/ubuntu/Documents/prak-sisop/modul2/HewanAir"
#define BASE_PATH "/home/ubuntu/Documents/prak-sisop/modul2/animals"

```

## Main function
For this question, I separated each point with a function, and the main function is only for calling the first function that we are going to use, and inside the parameter is the name of the zip file. 

```c
int main(){
        download_file("binatang.zip");

        return 0;
}

```

## A
For this question we were asked to make a program that will download a zip file from this link https://drive.google.com/uc?export=download&id=1oDgj5kSiDO0tlyS7-20uz7t20X3atwrq, and also make a program to unzip that downloaded file.

#### Download_file() function

```c
void download_file(char file_name[]){
        pid_t download_binatang, waiter;
        download_binatang = fork();

        if(download_binatang == 0){
                printf("Downloading binatang.zip file\n");

                char *args[] = {"/bin/wget", "wget","--quiet" , "https://drive.google.com/uc?export=download&id=1oDgj5kSiDO0tlyS7-20uz7t20X3atwrq", "-O", file_name, NULL};

                if(execv(args[0],args+1) == -1)
                {
                        perror("Downloading binatang.zip file failed!");
                        exit(EXIT_FAILURE);
                }
		        else {printf("Downloading success!\n");}
        }

	    else if(download_binatang > 0){
        waiter = wait(NULL);
        unzip();
        }

}
```
- This function specifically is used for downloading a zip file from the respective Googel Drive link, using `wget` via `execv()` to process the wget using args argument.
- The **--quiet** flag function is for not filling up the terminal when debugging.
- The if statement surrounding the **execv** is putted to catch for any errors and stopping the program if it happens to find one.
- Then use `wait(NULL)` on the parent process to wait for the child process to exit before proceeding to the next function, which is **unzip()**.

#### Unzip() function
```c
void unzip(){
        pid_t unzip_binatang, waiter;
        unzip_binatang = fork();

        if(unzip_binatang == 0){
		printf("Unzipping binatang file\n");

        	char *args[] = {"/bin/unzip", "unzip", "binatang.zip","-d", "animals", NULL};

		if(execv(args[0],args+1) == -1){

                	perror("unzip binatang failed!");
                	exit(EXIT_FAILURE);
        	}
		else { printf("Unzipping success!\n");}
	}

	else if(unzip_binatang > 0){
        	waiter = wait(NULL);
		random_animal();
	}
}

```

- This function specifically is used to extracting the downloaded zip file using `unzip` and `execv()`.
- The `char *args[]` argument is used to make `execv` calls for the `unzip` program with the **-d** flag and **animals** as the file name after the extraction.
- After the parent process finished waiting for the child process, it will then proceed to the **random_animal()** function.

## B

For this question, we were asked to make a program to choose a random file as the output.

```c
void random_animal() {
    char buf[PATH_MAX];
    DIR* dir;
    struct dirent *dir_ent;
    int number_files = 0;
    struct stat st;

    srand(time(NULL));

    dir = opendir(BASE_PATH);
    if (dir == NULL) {
        fprintf(stderr, "Error: Unable to open the animals directory.\n");
        return;
    }

    while ((dir_ent = readdir(dir)) != NULL) {
        char filepath[PATH_MAX];

        if (dir_ent->d_type == DT_REG) {
            snprintf(filepath, PATH_MAX, "%s/%s", BASE_PATH, dir_ent->d_name);
            if (stat(filepath, &st) == 0 && st.st_mode & S_IRUSR) {
                number_files++;
                if (rand() % number_files == 0) {
                    snprintf(buf, PATH_MAX, "%s", dir_ent->d_name);
                }
            }
        }
    }
    closedir(dir);

    if (number_files == 0) {
        fprintf(stderr, "Error: No readable files in animals directory.\n");
        return;
    }

    printf("The animal that will be looked after by Grape-kun is: %s\n", buf);

    make_directory();
}
```

- This function specifically is used to choose a random file from the `animals` directory and then make it as an output.
- The `rand() % number_files == 0` condition is used to choose the random file, so each time the program runs, the output file will be different.
- Then copy the name of the selected file to buf using `snprintf(buf, PATH_MAX, "%s", dir_ent->d_name);` function.
- Then the name of the selected file will be printed using `printf("The animal that will be looked after by Grape-kun is: %s\n", buf);`.
- After it printed out the file, it will proceed onto the **make_directory()** function.


## C
For this question we were asked to make a program where we have to make 3 directories which are **HewanDarat, HewanAmphibi,** and **HewanAir**. And then move the files according to which directory they belong to. For example, the file name is _shark_air_ then it will go to the HewanAir directory.

#### Make_directory() function
```c
void make_directory(){
	pid_t waiter, dirname1, dirname2, dirname3;
	dirname1 = fork();
	dirname2= fork();
	dirname3= fork();
        if (dirname1 > 0 && dirname2 > 0 && dirname3 > 0){
                waiter = wait(NULL);
                waiter = wait(NULL);
		filter_file();
        }

        else if (dirname1 == 0 && dirname2 == 0 && dirname3 == 0){
                char *args[5] = {"/bin/mkdir", "mkdir", "-p" , "HewanDarat", NULL};
                printf("HewanDarat directory created successfully.\n");

                if(execv(args[0],args+1) == -1){
                perror("making a new directory called HewanDarat is failed!");
                exit(EXIT_FAILURE);
                }
        }

       else if (dirname1 > 0 && dirname2 == 0 && dirname3 > 0){
                char *args[5] = {"/bin/mkdir", "mkdir", "-p" , "HewanAmphibi", NULL};
                printf("HewanAmphibi directory created successfully.\n");

                if(execv(args[0],args+1) == -1){
                perror("making a new directory called HewanAmphibi is failed!");
                exit(EXIT_FAILURE);
                }
        }

        else if (dirname1 > 0 && dirname2 > 0 && dirname3 == 0){
                char *args[5] = {"/bin/mkdir", "mkdir", "-p" , "HewanAir", NULL};
                printf("HewanAir directory created successfully.\n");

                if(execv(args[0],args+1) == -1){
                perror("making a new directory called HewanAir is failed!");
                exit(EXIT_FAILURE);
                }
        }
}

```
- This function is used to create 3 new directories called **_HewanDarat, HewanAir, HewanAmphibi_** using **mkdir** via **execv**. Like any other **"execv based function"**, the only difference is its **char *args[]**.
- This `args` argument makes `execv` calls for the `mkdir` program with the `-p` flag to ignore if a directory with the same name is presents.
- After the child process has exited, it will proceed onto the **filter_file()** function.

#### Filter_file() function
```c
void filter_file(){
    pid_t dar = 0, amp = 0, air = 0;
    int status;
    char source[1000], destination[1000];
    struct dirent *dp;
    DIR *dir = opendir(BASE_PATH);

    if (!dir){
        perror("Error opening directory");
        return;
    }

    while ((dp = readdir(dir)) != NULL){
        if (strstr(dp->d_name, "_") == NULL){
            continue;
        }
        sprintf(source, "%s/%s", BASE_PATH, dp->d_name);
        printf("Moving the %s file\n", dp->d_name);

        if (strstr(dp->d_name, "_darat") != NULL){
            sprintf(destination, DARAT_DIR "/%s", dp->d_name);
            dar = fork();
            if (dar == 0){
                execlp("mv", "mv", source, destination, NULL);
                perror("Error occurred while moving darat file");
                exit(EXIT_FAILURE);
            }
        }
        if (strstr(dp->d_name, "_amphibi") != NULL){
            sprintf(destination, AMPHIBI_DIR "/%s", dp->d_name);
            amp = fork();
            if (amp == 0){
                execlp("mv", "mv", source, destination, NULL);
                perror("Error occurred while moving amphibi file");
                exit(EXIT_FAILURE);
            }
        }
        if (strstr(dp->d_name, "_air") != NULL){
            sprintf(destination, AIR_DIR "/%s", dp->d_name);
            air = fork();
            if (air == 0){
                execlp("mv", "mv", source, destination, NULL);
                perror("Error occurred while moving air file");
                exit(EXIT_FAILURE);
            }
        }
    }

    closedir(dir);

    while ((waitpid(dar, &status, 0) > 0) || (waitpid(amp, &status, 0) > 0) || (waitpid(air, &status, 0) > 0));

    printf("All files have been moved successfully\n");

    zip_newdir();
}
```
- This function is specifically used to filter each file into the new directories depending on the file's last name.
- `strstr(dp->d_name, "_darat")` function is used to check if the file name contains `_darat` and if it is, it will be moved to the **HewanDarat** directory.
- `strstr(dp->d_name, "_amphibi")` function is used to check if the file name contains `_amphibi` and if it is, it will be moved to the **HewanAmphibi** directory.
- `strstr(dp->d_name, "_air")` function is used to check if the file name contains `_air` and if it is, it will be moved to the **HewanAir** directory.
- After the child process has exited, it will proceed onto the **zip_newdir()** function.


## D

And for this question, we were asked to make a program to zip those 3 new directories.
```c
void zip_newdir(){
        pid_t waiter, HewDar, HewAm, HewAir;
        HewDar = fork();
        HewAm = fork();
        HewAir = fork();

        if (HewDar == 0 && HewAm > 0 && HewAir > 0) {
                char *args[] = {"/bin/zip", "zip", "-r", "HewanDarat.zip", "HewanDarat", NULL};
                printf("HewanDarat.zip is created successfully!\n");

                if(execv(args[0],args+1) == -1) {
                        perror("zip failed!");
                        exit(EXIT_FAILURE);
                }

        }
        else if (HewDar > 0 && HewAm == 0 && HewAir > 0) {
                char *args[] = {"/bin/zip", "zip", "-r", "HewanAmphibi.zip", "HewanAmphibi", NULL};
                printf("HewanAmphibi.zip is created successfully!\n");

                if(execv(args[0],args+1) == -1) {
                        perror("zip failed!");
                        exit(EXIT_FAILURE);
                }

        }
        else if (HewDar > 0 && HewAm > 0 && HewAir == 0) {
                char *args[] = {"/bin/zip", "zip", "-r", "HewanAir.zip", "HewanAir", NULL};
                printf("HewanAir.zip is created successfully!\n");

                if(execv(args[0],args+1) == -1) {
                        perror("zip failed!");
                        exit(EXIT_FAILURE);
                }

        }
        else if (HewDar > 0 && HewAm > 0 && HewAir > 0) {
        waiter = wait(NULL);
        waiter = wait(NULL);
        }
}

```
- This function is specifically used to zip the 3 directories which are **_HewanDarat, HewanAir_** and **_HewanAmphibi_** using **zip** via **execv**. Like any other **execv based function**, the only real difference is its **char *args[]**.
- The `char *args[]` argument make `execv` call for the `zip` program with the **-r** flag, `HewanDarat(Air)(Amphibi).zip` as the file name. 

## Qustion 2
Sucipto adalah seorang seniman terkenal yang berasal dari Indonesia. Karya nya sudah terkenal di seluruh dunia, dan lukisannya sudah dipajang di berbagai museum mancanegara. Tetapi, akhir-akhir ini sucipto sedang terkendala mengenai ide lukisan ia selanjutnya. Sebagai teman yang jago sisop, bantu sucipto untuk melukis dengan mencarikannya gambar-gambar di internet sebagai referensi !

A. Pertama-tama, buatlah sebuah folder khusus, yang dalamnya terdapat sebuah program C yang per 30 detik membuat sebuah folder dengan nama timestamp [YYYY-MM-dd_HH:mm:ss].

B. Tiap-tiap folder lalu diisi dengan 15 gambar yang di download dari https://picsum.photos/ , dimana tiap gambar di download setiap 5 detik. Tiap gambar berbentuk persegi dengan ukuran (t%1000)+50 piksel dimana t adalah detik Epoch Unix. Gambar tersebut diberi nama dengan format timestamp [YYYY-mm-dd_HH:mm:ss].

C. Agar rapi, setelah sebuah folder telah terisi oleh 15 gambar, folder akan di zip dan folder akan di delete(sehingga hanya menyisakan .zip).

D. Karena takut program tersebut lepas kendali, Sucipto ingin program tersebut men-generate sebuah program "killer" yang siap di run(executable) untuk menterminasi semua operasi program tersebut. Setelah di run, program yang menterminasi ini lalu akan mendelete dirinya sendiri.

E. Buatlah program utama bisa dirun dalam dua mode, yaitu MODE_A dan MODE_B. untuk mengaktifkan MODE_A, program harus dijalankan dengan argumen -a. Untuk MODE_B, program harus dijalankan dengan argumen -b. Ketika dijalankan dalam MODE_A, program utama akan langsung menghentikan semua operasinya ketika program killer dijalankan. Untuk MODE_B, ketika program killer dijalankan, program utama akan berhenti tapi membiarkan proses di setiap folder yang masih berjalan sampai selesai(semua folder terisi gambar, terzip lalu di delete).

Catatan :

1. Tidak boleh menggunakan system()
2. Proses berjalan secara daemon
3. Proses download gambar pada beberapa folder dapat berjalan secara bersamaan (overlapping)

```c
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <sys/stat.h>
#include <unistd.h>
#include <sys/mman.h>
#include <sys/wait.h>


#define IMAGE_NAME "%Y-%m-%d_%H:%M:%S.jpg"

void create_folder() {
    // declare folder name
    char foldername[30];
    int status;
    // take current time
    time_t now = time(NULL);
    struct tm *tm = localtime(&now);
    // use current time as folder name
    strftime(foldername, sizeof(foldername), "folder_%Y-%m-%d_%H:%M:%S", tm);
    // make folder
    mkdir(foldername, 0700);
    chdir(foldername);
    wait(&status);
}

// void create_zip(){
    
// }

void download_images() {
    int status;
    pid_t pid = fork();
    
    for(int i=0; i<15; i++){
        // take the time
        time_t timeepoch;
        time_t now = time(NULL);
        time_t current_time;
        struct tm *tm = localtime(&now);
        // take url
        char url[] = "https://picsum.photos/";
        char file_name[30];
        // get time
        time(&current_time);
        // use time as file name
        strftime(file_name, sizeof(file_name), "%Y-%m-%d_%H:%M:%S.jpg", tm);
        // time epoch
        timeepoch = time(NULL);
        int timeepochsize = (timeepoch%1000)+50;
        char timeepochstr[60];

        sprintf(timeepochstr, "%d", timeepochsize);
        // unite link with epoch
        strcat(url, timeepochstr);
        // fork another child process
        pid = fork();
        // check if pid is 0
        if(pid == 0){
            // download
            char *command[]={"wget",url, "-O", file_name, NULL};
            execvp(command[0],command);
            printf("Downloading...");
        }
        sleep(5);
    }
    wait(&status);
}

void kill_script(){
    // create kiler.sh
    FILE* script_file = fopen("killer.sh", "w");
    if (script_file == NULL) {
        perror("Failed to create killer");
        exit(EXIT_FAILURE);
    }

    fprintf(script_file, "#!/bin/sh\n");
    fprintf(script_file, "pid=$(pgrep -f %s)\n", __FILE__);
    // check pid not empty
    fprintf(script_file, "if [ ! -z \"$pid\" ]; then\n");
    // kill the program
    fprintf(script_file, "    kill $pid\n");
    fprintf(script_file, "fi\n");
    // remove the file
    fprintf(script_file, "rm -f killer.sh\n");

}


int main() {
    kill_script();
    while(1) {
        pid_t pid = fork();
        if (pid == 0) { 
            create_folder();
            download_images();
            exit(0);
        }
        else if (pid < 0) {
            perror("fork error");
            exit(0);
        }
        else {
            sleep(15); 
        }
    }

    return 0;
}


```

# Explanation
The question ask us to create a program to download 15 image with a certain size, a certain name, and put it in a folder with a certain folder name.The program will create a folder every 15 second and download image every 5 second.
### Create Folder
The first thing to do is to get the current time at the moment by using time_t and localtime(&now). Using the data we get, we use it for the folder name. We then create the folder using mkdir and to make sure the file is downloaded into the folder we cd into the folder.
### Download
We want to download the file by using a website which will find an image with an inputted size on the link. We prepare the url in string. In this case the size is (t%1000)+50 pixel where t is the second in Epoch Unix. First we create a child and then a loop 15 times. Then just like in create folder,  we take the current time and use it as the file name. Then using the current time, we convert it to epoch unix and convert it into a string. Then using strcat we connect the url and the epoch unix as both of them are already a string. Next using wget we download the file. lastly we pause for 5 second using wait(5).
### Killer
The question requires us to create a killer that will stop the program and then the killer then will delete itself. The first thing to do is to create the file, called "killer.sh". Then we check if the pid is not NULL and then kill the program. Lastly we delete the program using "rm".

# No 3


## Question
Ten Hag adalah seorang pelatih Ajax di Liga Belanda. Suatu hari, Ten Hag mendapatkan tawaran untuk menjadi manajer Manchester United. Karena Ten Hag masih mempertimbangkan tawaran tersebut, ia ingin mengenal para pemain yang akan dilatih kedepannya. Dikarenakan Ten Hag hanya mendapatkan url atau link database mentah para pemain bola, maka ia perlu melakukan klasifikasi pemain Manchester United. Bantulah Ten Hag untuk mengenal para pemain Manchester United tersebut hanya dengan 1 Program C bernama “filter.c”

>- Pertama-tama, Program filter.c akan mengunduh file yang berisikan database para pemain bola. Kemudian dalam program yang sama diminta dapat melakukan extract “players.zip”. Lalu hapus file zip tersebut agar tidak memenuhi komputer Ten Hag.
>- Dikarenakan database yang diunduh masih data mentah. Maka bantulah Ten Hag untuk menghapus semua pemain yang bukan dari Manchester United yang ada di directory.  
>- Setelah mengetahui nama-nama pemain Manchester United, Ten Hag perlu untuk mengkategorikan pemain tersebut sesuai dengan posisi mereka dalam waktu bersamaan dengan 4 proses yang berbeda. Untuk kategori folder akan menjadi 4 yaitu Kiper, Bek, Gelandang, dan Penyerang.
>- Setelah mengkategorikan anggota tim Manchester United, Ten Hag memerlukan Kesebelasan Terbaik untuk menjadi senjata utama MU berdasarkan rating terbaik dengan wajib adanya kiper, bek, gelandang, dan penyerang. (Kiper pasti satu pemain). Untuk output nya akan menjadi ```Formasi_[jumlah bek]-[jumlah gelandang]-[jumlah penyerang].txt``` dan akan ditaruh di ```/home/[users]/```

>- Catatan:
> > Format nama file yang akan diunduh dalam zip dan isi txt formasi berupa [nama]_[tim]_[posisi]_[rating].png
> > Tidak boleh menggunakan system()
> > Tidak boleh memakai function C mkdir() ataupun rename().
> > Gunakan exec() dan fork().
> > Directory “.” dan “..” tidak termasuk yang akan dihapus.
> > Untuk poin d DIWAJIBKAN membuat fungsi bernama buatTim(int, int, int), dengan input 3 value integer dengan urutan bek, gelandang, dan striker.

source code:

```c
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>
#include <dirent.h>
#include <string.h>
#include <fcntl.h>

#define PATH_SIZE 1024
#define BUF_SIZE 1024

typedef struct PlayerName{
    char fileName[128];
    int rating;
} PlayerName;

PlayerName *parseDirectory(char *directory){
    struct dirent *entry;
    static PlayerName players[16];
    DIR *dir = opendir(directory);

    if (directory == NULL){
        return NULL;
    }

    int i = 0;

    while((entry = readdir(dir)) != NULL){
        char *fileName = entry->d_name, *token;
        if(!(strcmp(fileName, ".") == 0) && !(strcmp(fileName, "..")) == 0){
            strcpy(players[i].fileName, fileName);
            token = strtok(fileName, "_");
            token = strtok(NULL, "_");
            token = strtok(NULL, "_");
            token = strtok(NULL, "_");
            sscanf(token, "%d", &players[i].rating);
            i++;
        }
    }
    strcpy(players[i].fileName, "END");
    players[i].rating = -1;

    closedir(dir);
    return players;
}

void dowload() {
    pid_t pid = fork();
    if (pid == -1) {
        perror("ERROR");
        exit(EXIT_FAILURE);
    }
    if (pid == 0){
        char *args[] = {
            "wget", "-O", "players.zip", "https://drive.google.com/u/0/uc?id=1zEAneJ1-0sOgt13R1gL4i1ONWfKAtwBF&export=download", NULL};
            execvp(args[0], args);
            perror("ERROR");
            exit(EXIT_FAILURE);
        }
    else {
        wait(NULL);
    }
 }

void extract() {
    pid_t pid = fork();
    if (pid == -1) {
        perror("ERROR");
        exit(EXIT_FAILURE);
    }
    if (pid == 0){
        char *args[] = {"unzip", "players.zip",  NULL};
            execvp(args[0], args);
            perror("ERROR");
            exit(EXIT_FAILURE);
        }
    else {
        wait(NULL);
    }
}

void FilterMU() {
    DIR *d;
    struct dirent *dir;
    d = opendir(".");
    if (d) {
        while ((dir = readdir(d)) != NULL){
            if (dir->d_type == DT_DIR && strcmp(dir->d_name, "players") == 0) {
                DIR *d_players = opendir("players");
                if (d_players){
                    struct dirent *dir_players;
                    while ((dir_players = readdir(d_players)) != NULL)
                    {
                        if (dir_players->d_type == DT_REG) {
                            char *fileName = dir_players->d_name;
                            if(strstr(fileName, "ManUtd") == NULL){
                                char filePath[100] = "players/";
                                strcat(filePath, fileName);
                                remove(filePath);
                            }
                        }
                    }
                    closedir(d_players);
                }
            }
        }
        closedir(d);
    }
}

void makeFolder(){
    
    pid_t pid = fork();
    if (pid == -1) {
        perror("ERROR");
        exit(EXIT_FAILURE);
    }
    if (pid == 0){
        char *args1[] = {"mkdir", "players/Gelandang", "players/Kiper", "players/Bek", "players/Penyerang", NULL};
        execvp(args1[0], args1);
        perror("ERROR");
        exit(EXIT_FAILURE);
        }
    else {
        wait(NULL);
    }

}

void categorize(){
    
    DIR *d;
    struct dirent *dir;
    d = opendir(".");
    pid_t pid = fork();

    if(pid == -1){
        perror("ERROR");
        exit(EXIT_FAILURE);
    }

    if(pid == 0){
        if (d) {
            while ((dir = readdir(d)) != NULL){
                if (dir->d_type == DT_DIR && strcmp(dir->d_name, "players") == 0) {
                    DIR *d_players = opendir("players");
                    if (d_players){
                        struct dirent *dir_players;
                        while ((dir_players = readdir(d_players)) != NULL)
                        {
                            if (dir_players->d_type == DT_REG) {
                                char *fileName = dir_players->d_name;
                                if(strstr(fileName, "Gelandang") != NULL && strstr(fileName, "png") != NULL){
                                    char filePath[100] = "players/";
                                    char *file = strcat(filePath, fileName);
                                    char *args[] = {"mv", file, "players/Gelandang", NULL};
                                    pid_t pid = fork();
                                    
                                    if(pid == 0){
                                    execvp("mv", args);
                                    }
                                    
                                    else{
                                    wait(NULL);
                                    }
                                    
                                }
                                else if(strstr(fileName, "Kiper") != NULL && strstr(fileName, "png") != NULL){
                                    char filePath[100] = "players/";
                                    char *file = strcat(filePath, fileName);
                                    char *args[] = {"mv", file, "players/Kiper", NULL};
                                    pid_t pid = fork();
                                    
                                    if(pid == 0){
                                    execvp("mv", args);
                                    }
                                    
                                    else{
                                    wait(NULL);
                                    }
                                    
                                }
                                else if(strstr(fileName, "Bek") != NULL && strstr(fileName, "png") != NULL){
                                    char filePath[100] = "players/";
                                    char *file = strcat(filePath, fileName);
                                    char *args[] = {"mv", file, "players/Bek", NULL};
                                    pid_t pid = fork();
                                    
                                    if(pid == 0){
                                    execvp("mv", args);
                                    }
                                    
                                    else{
                                    wait(NULL);
                                    }
                                    
                                }
                                else if(strstr(fileName, "Penyerang") != NULL && strstr(fileName, "png") != NULL){
                                    char filePath[100] = "players/";
                                    char *file = strcat(filePath, fileName);
                                    char *args[] = {"mv", file, "players/Penyerang", NULL};
                                    pid_t pid = fork();
                                    
                                    if(pid == 0){
                                    execvp("mv", args);
                                    }
                                    
                                    else{
                                    wait(NULL);
                                    }
                                    
                                }
                            }
                        }
                        closedir(d_players);
                    }
                }
            }
            closedir(d);
        }
    
}
    

}

int getArrSize(PlayerName *arr) {
    int i = 0;
    while (!(strcmp(arr[i].fileName, "END") == 0) && (arr[i].rating != -1)) {
        i++;
    }
    return i;
}

void bubbleSort(PlayerName *arr, int n) {
    int swapped;
    for (int i = 0; i < n - 1; i++) {
        swapped = 0;

        for (int j = 0; j < n - i - 1; j++) {
           
            if (arr[j].rating < arr[j + 1].rating) {
           
                PlayerName tmp = arr[j];
                arr[j] = arr[j + 1];
                arr[j + 1] = tmp;
            }
        }
        if (swapped) break;
    }
}

void buatTim(int bek, int gelandang, int penyerang) {

    char outputFile[32] = "";
    snprintf(outputFile + strlen(outputFile), 32 - strlen(outputFile),
             "Formasi_%d-%d-%d.txt", bek, gelandang, penyerang);
    FILE *f = fopen(outputFile, "w");

    PlayerName *kiperArr = parseDirectory("players/Kiper");
    bubbleSort(kiperArr, getArrSize(kiperArr));

    fwrite(kiperArr[0].fileName, sizeof(char), strlen(kiperArr[0].fileName), f);
    fwrite("\n", sizeof(char), strlen("\n"), f);

    PlayerName *bekArr = parseDirectory("players/Bek");
    bubbleSort(bekArr, getArrSize(bekArr));

    for (int i = 0; i < bek; i++) {
        fwrite(bekArr[i].fileName, sizeof(char), strlen(bekArr[i].fileName), f);
        fwrite("\n", sizeof(char), strlen("\n"), f);
    }

    PlayerName *gelandangArr = parseDirectory("players/Gelandang");
    bubbleSort(gelandangArr, getArrSize(gelandangArr));

    for (int i = 0; i < gelandang; i++) {
        fwrite(gelandangArr[i].fileName, sizeof(char),
               strlen(gelandangArr[i].fileName), f);
        fwrite("\n", sizeof(char), strlen("\n"), f);
    }

    PlayerName *penyerangArr = parseDirectory("players/Penyerang");
    bubbleSort(penyerangArr, getArrSize(penyerangArr));

    for (int i = 0; i < penyerang; i++) {
        fwrite(penyerangArr[i].fileName, sizeof(char),
               strlen(penyerangArr[i].fileName), f);
        fwrite("\n", sizeof(char), strlen("\n"), f);
    }

    fclose(f);
}

int main(){
    dowload();
    extract();
    unlink("players.zip");
    FilterMU();
    makeFolder();
    categorize();
    buatTim(4,3,3);
    return 0;
    
}

```

# Explanation

## Main Function
In the main function, all we did was call all the function that answers the question. Here is a snippet of the main function

```c
int main(){
    dowload();
    extract();
    unlink("players.zip");
    FilterMU();
    makeFolder();
    categorize();
    buatTim(4,3,3);
    return 0;
    
}
```

## A
The question told us to download football players database using a C file. The link to download the database is provided within the question. The question also said to extract the zip file and after extracting, delete the zip file so it doesn't fill up unecessary space. For this point we used the ```download()```, ```extract()```, to download and extract the .zip file and we use ```unlink(players.zip)``` to delete the zip file after extracting the contents.
>- Here is the code for the ```download()``` function:
```c
    void dowload() {
    pid_t pid = fork();
    if (pid == -1) {
        perror("ERROR");
        exit(EXIT_FAILURE);
    }
    if (pid == 0){
        char *args[] = {
            "wget", "-O", "players.zip", "https://drive.google.com/u/0/uc?id=1zEAneJ1-0sOgt13R1gL4i1ONWfKAtwBF&export=download", NULL};
            execvp(args[0], args);
            perror("ERROR");
            exit(EXIT_FAILURE);
        }
    else {
        wait(NULL);
    }
 }
```
In this function, we make a child process that download the .zip file using ```wget``. However, the link in the question is "https://drive.google.com/file/d/1zEAneJ1-0sOgt13R1gL4i1ONWfKAtwBF/view". This link format is not for downloading. Therefore, we change it to "https://drive.google.com/u/0/uc?id=1zEAneJ1-0sOgt13R1gL4i1ONWfKAtwBF&export=download" in the function.

>- Here is the code for the ```extract()``` function:
```c
    void extract() {
        pid_t pid = fork();
        if (pid == -1) {
            perror("ERROR");
            exit(EXIT_FAILURE);
        }
        if (pid == 0){
            char *args[] = {"unzip", "players.zip",  NULL};
                execvp(args[0], args);
                perror("ERROR");
                exit(EXIT_FAILURE);
            }
        else {
            wait(NULL);
        }
    }
```
This function works in a similiar manner as the one before. The difference is that this function execute the ```unzip``` command to extract the content inside the .zip file, rather than using ```wget``` to download file.

## B
The question told us to filter out the players that are not from Manchester United. This means that we have to delete all the file that doesn't have "ManUtd" in its name. To do that we use the ```FilterMU()``` function.
 >- Here is the code for the ```FilterMU()``` function:
```c
    void FilterMU() {
    DIR *d;
    struct dirent *dir;
    d = opendir(".");
    if (d) {
        while ((dir = readdir(d)) != NULL){
            if (dir->d_type == DT_DIR && strcmp(dir->d_name, "players") == 0) {
                DIR *d_players = opendir("players");
                if (d_players){
                    struct dirent *dir_players;
                    while ((dir_players = readdir(d_players)) != NULL)
                    {
                        if (dir_players->d_type == DT_REG) {
                            char *fileName = dir_players->d_name;
                            if(strstr(fileName, "ManUtd") == NULL){
                                char filePath[100] = "players/";
                                strcat(filePath, fileName);
                                remove(filePath);
                            }
                        }
                    }
                    closedir(d_players);
                }
            }
        }
        closedir(d);
    }
}
```
This function get inside the "players" directory which is created when we extract the .zip file using ```opendir()```, and then check its content. This function check the players directory using ```if (dir->d_type == DT_DIR && strcmp(dir->d_name, "players") == 0)```. This function then uses ```if(strstr(fileName, "ManUtd") == NULL)``` to then check whether or not the files inside the players directory has "ManUtd" in its name. if the file does not have "ManUtd" in its name, it will remove such files using ```remove()```.

## C

The question told us to separate players file based on each position. This means that there will be four category that corresponds to the players position. These 4 categories are "Bek", "Penyerang", "Gelandang", and "Kiper". To answer this, we first make new directories inside the "players" directory. We use the ```makeFolder()``` function.

>- Here is the code for ```makeFolder()``` function:
```c
void makeFolder(){
    
    pid_t pid = fork();
    if (pid == -1) {
        perror("ERROR");
        exit(EXIT_FAILURE);
    }
    if (pid == 0){
        char *args1[] = {"mkdir", "players/Gelandang", "players/Kiper", "players/Bek", "players/Penyerang", NULL};
        execvp(args1[0], args1);
        perror("ERROR");
        exit(EXIT_FAILURE);
        }
    else {
        wait(NULL);
    }

}
```

This function execute the ```mkdir``` command using child process. It will then make 4 extra directories inside the "players" directory. Then, we need to again check the filenames for keywords like "Penyerang", or "Bek" that indicates the position of the player. To do that, we uses the ```categorize()``` function.
>- Here is the code for ```categorize()``` function:
```c
void categorize(){
    
    DIR *d;
    struct dirent *dir;
    d = opendir(".");
    pid_t pid = fork();

    if(pid == -1){
        perror("ERROR");
        exit(EXIT_FAILURE);
    }

    if(pid == 0){
        if (d) {
            while ((dir = readdir(d)) != NULL){
                if (dir->d_type == DT_DIR && strcmp(dir->d_name, "players") == 0) {
                    DIR *d_players = opendir("players");
                    if (d_players){
                        struct dirent *dir_players;
                        while ((dir_players = readdir(d_players)) != NULL)
                        {
                            if (dir_players->d_type == DT_REG) {
                                char *fileName = dir_players->d_name;
                                if(strstr(fileName, "Gelandang") != NULL && strstr(fileName, "png") != NULL){
                                    char filePath[100] = "players/";
                                    char *file = strcat(filePath, fileName);
                                    char *args[] = {"mv", file, "players/Gelandang", NULL};
                                    pid_t pid = fork();
                                    
                                    if(pid == 0){
                                    execvp("mv", args);
                                    }
                                    
                                    else{
                                    wait(NULL);
                                    }
                                    
                                }
                                else if(strstr(fileName, "Kiper") != NULL && strstr(fileName, "png") != NULL){
                                    char filePath[100] = "players/";
                                    char *file = strcat(filePath, fileName);
                                    char *args[] = {"mv", file, "players/Kiper", NULL};
                                    pid_t pid = fork();
                                    
                                    if(pid == 0){
                                    execvp("mv", args);
                                    }
                                    
                                    else{
                                    wait(NULL);
                                    }
                                    
                                }
                                else if(strstr(fileName, "Bek") != NULL && strstr(fileName, "png") != NULL){
                                    char filePath[100] = "players/";
                                    char *file = strcat(filePath, fileName);
                                    char *args[] = {"mv", file, "players/Bek", NULL};
                                    pid_t pid = fork();
                                    
                                    if(pid == 0){
                                    execvp("mv", args);
                                    }
                                    
                                    else{
                                    wait(NULL);
                                    }
                                    
                                }
                                else if(strstr(fileName, "Penyerang") != NULL && strstr(fileName, "png") != NULL){
                                    char filePath[100] = "players/";
                                    char *file = strcat(filePath, fileName);
                                    char *args[] = {"mv", file, "players/Penyerang", NULL};
                                    pid_t pid = fork();
                                    
                                    if(pid == 0){
                                    execvp("mv", args);
                                    }
                                    
                                    else{
                                    wait(NULL);
                                    }
                                    
                                }
                            }
                        }
                        closedir(d_players);
                    }
                }
            }
            closedir(d);
        }
    
}
    

}
```
This function works like the ```FilterMU()``` in a way that they both open up directories and find keywords using ```strstr```. The difference is that this function find keywords, then they make a child process that executes ```mv``` to move the files that has certain keywords into their category folder. for example the files that has the keyword "Penyerang" would then be moved to the directory that is named "Penyerang" inside the "players" directory.
>- Example of moving the "Penyerang":
```c
                                else if(strstr(fileName, "Penyerang") != NULL && strstr(fileName, "png") != NULL){
                                    char filePath[100] = "players/";
                                    char *file = strcat(filePath, fileName);
                                    char *args[] = {"mv", file, "players/Penyerang", NULL};
                                    pid_t pid = fork();
                                    
                                    if(pid == 0){
                                    execvp("mv", args);
                                    }
                                    
                                    else{
                                    wait(NULL);
                                    }
                                    
                                }
```

## D
The question tell us to filter the ones that have high rating to tell which formation is the best for Manchester United. After that, store them in a .txt file with the name format ```Formasi_[jumlah bek]-[jumlah gelandang]-[jumlah penyerang].txt```. To achieve this, we first need to make the ```PlayerName``` struct, and then parse the struct through the directories.
>- Here is the code for the struct and the parsing:
```c
typedef struct PlayerName{
    char fileName[128];
    int rating;
} PlayerName;

PlayerName *parseDirectory(char *directory){
    struct dirent *entry;
    static PlayerName players[16];
    DIR *dir = opendir(directory);

    if (directory == NULL){
        return NULL;
    }

    int i = 0;

    while((entry = readdir(dir)) != NULL){
        char *fileName = entry->d_name, *token;
        if(!(strcmp(fileName, ".") == 0) && !(strcmp(fileName, "..")) == 0){
            strcpy(players[i].fileName, fileName);
            token = strtok(fileName, "_");
            token = strtok(NULL, "_");
            token = strtok(NULL, "_");
            token = strtok(NULL, "_");
            sscanf(token, "%d", &players[i].rating);
            i++;
        }
    }
    strcpy(players[i].fileName, "END");
    players[i].rating = -1;

    closedir(dir);
    return players;
}
```
Then, to iterate and filter the rating of the players we use these following functions:
>- ```getArrSize()```
```c
int getArrSize(PlayerName *arr) {
    int i = 0;
    while (!(strcmp(arr[i].fileName, "END") == 0) && (arr[i].rating != -1)) {
        i++;
    }
    return i;
}


```
>- ```bubbleSort()```
```c
void bubbleSort(PlayerName *arr, int n) {
    int swapped;
    for (int i = 0; i < n - 1; i++) {
        swapped = 0;

        for (int j = 0; j < n - i - 1; j++) {
           
            if (arr[j].rating < arr[j + 1].rating) {
           
                PlayerName tmp = arr[j];
                arr[j] = arr[j + 1];
                arr[j + 1] = tmp;
            }
        }
        if (swapped) break;
    }
}
```

Lasltly, to write into the txt files we use the ```buatTim()``` function
>- Here is the ```buatTim()``` code:
```c
void buatTim(int bek, int gelandang, int penyerang) {

    char outputFile[32] = "";
    snprintf(outputFile + strlen(outputFile), 32 - strlen(outputFile),
             "Formasi_%d-%d-%d.txt", bek, gelandang, penyerang);
    FILE *f = fopen(outputFile, "w");

    PlayerName *kiperArr = parseDirectory("players/Kiper");
    bubbleSort(kiperArr, getArrSize(kiperArr));

    fwrite(kiperArr[0].fileName, sizeof(char), strlen(kiperArr[0].fileName), f);
    fwrite("\n", sizeof(char), strlen("\n"), f);

    PlayerName *bekArr = parseDirectory("players/Bek");
    bubbleSort(bekArr, getArrSize(bekArr));

    for (int i = 0; i < bek; i++) {
        fwrite(bekArr[i].fileName, sizeof(char), strlen(bekArr[i].fileName), f);
        fwrite("\n", sizeof(char), strlen("\n"), f);
    }

    PlayerName *gelandangArr = parseDirectory("players/Gelandang");
    bubbleSort(gelandangArr, getArrSize(gelandangArr));

    for (int i = 0; i < gelandang; i++) {
        fwrite(gelandangArr[i].fileName, sizeof(char),
               strlen(gelandangArr[i].fileName), f);
        fwrite("\n", sizeof(char), strlen("\n"), f);
    }

    PlayerName *penyerangArr = parseDirectory("players/Penyerang");
    bubbleSort(penyerangArr, getArrSize(penyerangArr));

    for (int i = 0; i < penyerang; i++) {
        fwrite(penyerangArr[i].fileName, sizeof(char),
               strlen(penyerangArr[i].fileName), f);
        fwrite("\n", sizeof(char), strlen("\n"), f);
    }

    fclose(f);
}
```


## Qustion 4
Banabil adalah seorang mahasiswa yang rajin kuliah dan suka belajar. Namun naasnya Banabil salah mencari teman, dia diajak ke toko mainan oleh teman-temannya dan teracuni untuk membeli banyak sekali mainan dan kebingungan memilih mainan mana yang harus dibeli. Hal tersebut menyebabkan Banabil kehilangan fokus dalam pengerjaan tugas-tugas yang diberikan oleh dosen nya. Untuk mengembalikan fokusnya, Banabil harus melatih diri sendiri dalam membuat program untuk menjalankan script bash yang menyerupai crontab dan menggunakan bahasa C karena baru dipelajari olehnya.

Untuk menambah tantangan agar membuatnya semakin terfokus, Banabil membuat beberapa ketentuan custom yang harus dia ikuti sendiri. Ketentuan tersebut berupa:

-Banabil tidak ingin menggunakan fungsi system(), karena terlalu mudah.

-Dalam pelatihan fokus time managementnya, 
Banabil harus bisa membuat program yang dapat menerima argumen berupa Jam (0-23), Menit (0-59), Detik (0-59), Tanda asterisk [ * ] (value bebas), serta path file .sh.

-Dalam pelatihan fokus untuk ketepatan pilihannya, Banabil ingin programnya dapat mengeluarkan pesan “error” apabila argumen yang diterima program tidak sesuai. Pesan error dapat dibentuk sesuka hati oleh pembuat program. terserah bagaimana, yang penting tulisan error.

-Terakhir, dalam pelatihan kesempurnaan fokusnya, Banabil ingin program ini berjalan dalam background dan hanya menerima satu config cron.

-Bonus poin apabila CPU state minimum.


Contoh untuk run: /program \* 44 5 /home/Banabil/programcron.sh

Source code for No. 4:

```c
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <time.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/wait.h>

// execute the file on path
void call_path(const char *program_path)
{
    pid_t pid = fork();
    // Child procss
    if (pid == 0)
    {
        execl(program_path, program_path, NULL);
        exit(0);
    }
    // Parent 
    else if (pid > 0)
    {
        waitpid(pid, NULL, 0);
    }
}

int main(int argc, char *argv[])
{
    pid_t pid, sid;
    int fd, hh, mm, ss, ret;
    time_t now;
    struct tm time_info;
    const char *program_path;

    // Checking the number of argument
    if (argc != 5)
    {
        fprintf(stderr, "Error, invalid argument: ./mainan hh mm ss program_path\n");
        exit(EXIT_FAILURE);
    }

    // Parsing the "*" and checking the format hour, minute, second
    if (strcmp(argv[1], "*") == 0)
    {
        hh = -1;
    }
    // check hour between 0-23
    else
    {
        ret = sscanf(argv[1], "%d", &hh);
        if (ret != 1 || hh < 0 || hh > 23)
        {
            fprintf(stderr, "Invalid hour: %s\n", argv[1]);
            exit(EXIT_FAILURE);
        }
    }
    if (strcmp(argv[2], "*") == 0)
    {
        mm = -1;
    }
    else
    //check minute
    {
        ret = sscanf(argv[2], "%d", &mm);
        if (ret != 1 || mm < 0 || mm > 59)
        {
            fprintf(stderr, "Invalid minute: %s\n", argv[2]);
            exit(EXIT_FAILURE);
        }
    }
    if (strcmp(argv[3], "*") == 0)
    {
        ss = -1;
    }
    //check second
    else
    {
        ret = sscanf(argv[3], "%d", &ss);
        if (ret != 1 || ss < 0 || ss > 59)
        {
            fprintf(stderr, "Invalid second: %s\n", argv[3]);
            exit(EXIT_FAILURE);
        }
    }

    // taking the path argument
    program_path = argv[4];

    // Forking parent
    pid = fork();
    if (pid < 0)
    {
        exit(EXIT_FAILURE);
    }
    if (pid > 0)
    {
        exit(EXIT_SUCCESS);
    }

    umask(0);

    // SID for child
    sid = setsid();
    if (sid < 0)
    {
        exit(EXIT_FAILURE);
    }

    close(STDIN_FILENO);
    close(STDOUT_FILENO);
    close(STDERR_FILENO);

    //  Write the log in append mode
    FILE *fp = fopen("/home/fadhlan/log.txt", "a");
    if (fp == NULL)
    {
        perror("fopen");
        exit(EXIT_FAILURE);
    }

    while (1)
    {
        // Add current time
        now = time(NULL);
        localtime_r(&now, &time_info);

        // checking the time 
        if ((hh == -1 || hh == time_info.tm_hour) &&
            (mm == -1 || mm == time_info.tm_min) &&
            (ss == -1 || ss == time_info.tm_sec))
        {
            // call function to run the path file
            call_path(program_path);
            // To tell if the file successfully executed
            dprintf(fd, "Program successfully executed at %s", ctime(&now));
        }

        // Sleep the program so that i run every second
        sleep(1);
    }

    fclose(fp);

    return EXIT_SUCCESS;
}     

```
# Explanation
The question wants us to create a program which will run a script bash that is similar to a crontab while using a c program.
The program will receive an hour(0-23), minute(0-59), second (0-59), an asterisk symbol (*) with free value. and a path file.sh.
The program will give an output "error" if the given argument is not in accordance to the requirement above.
Lastly, the program must run on the background and only receive one config cron.

#### call_path() function
```c
void call_path(const char *program_path)
{
    pid_t pid = fork();
    // Child procss
    if (pid == 0)
    {
        execl(program_path, program_path, NULL);
        exit(0);
    }
    // Parent 
    else if (pid > 0)
    {
        waitpid(pid, NULL, 0);
    }
}
```
- This function is specifically used for executing a program or file at a given path.
- It first forks a child and then executes the program using **execl** system call in the child process.

#### main() function
```c
int main(int argc, char *argv[])
{
    pid_t pid, sid;
    int fd, hh, mm, ss, ret;
    time_t now;
    struct tm time_info;
    const char *program_path;

    // Checking the number of argument
    if (argc != 5)
    {
        fprintf(stderr, "Error, invalid argument: ./mainan hh mm ss program_path\n");
        exit(EXIT_FAILURE);
    }

    // Parsing the "*" and checking the format hour, minute, second
    if (strcmp(argv[1], "*") == 0)
    {
        hh = -1;
    }
    // check hour between 0-23
    else
    {
        ret = sscanf(argv[1], "%d", &hh);
        if (ret != 1 || hh < 0 || hh > 23)
        {
            fprintf(stderr, "Invalid hour: %s\n", argv[1]);
            exit(EXIT_FAILURE);
        }
    }
    if (strcmp(argv[2], "*") == 0)
    {
        mm = -1;
    }
    else
    //check minute
    {
        ret = sscanf(argv[2], "%d", &mm);
        if (ret != 1 || mm < 0 || mm > 59)
        {
            fprintf(stderr, "Invalid minute: %s\n", argv[2]);
            exit(EXIT_FAILURE);
        }
    }
    if (strcmp(argv[3], "*") == 0)
    {
        ss = -1;
    }
    //check second
    else
    {
        ret = sscanf(argv[3], "%d", &ss);
        if (ret != 1 || ss < 0 || ss > 59)
        {
            fprintf(stderr, "Invalid second: %s\n", argv[3]);
            exit(EXIT_FAILURE);
        }
    }

    // taking the path argument
    program_path = argv[4];

    // Forking parent
    pid = fork();
    if (pid < 0)
    {
        exit(EXIT_FAILURE);
    }
    if (pid > 0)
    {
        exit(EXIT_SUCCESS);
    }

    umask(0);

    // SID for child
    sid = setsid();
    if (sid < 0)
    {
        exit(EXIT_FAILURE);
    }

    close(STDIN_FILENO);
    close(STDOUT_FILENO);
    close(STDERR_FILENO);

    //  Write the log in append mode
    FILE *fp = fopen("/home/fadhlan/log.txt", "a");
    if (fp == NULL)
    {
        perror("fopen");
        exit(EXIT_FAILURE);
    }

    while (1)
    {
        // Add current time
        now = time(NULL);
        localtime_r(&now, &time_info);

        // checking the time 
        if ((hh == -1 || hh == time_info.tm_hour) &&
            (mm == -1 || mm == time_info.tm_min) &&
            (ss == -1 || ss == time_info.tm_sec))
        {
            // call function to run the path file
            call_path(program_path);
            // To tell if the file successfully executed
            dprintf(fd, "Program successfully executed at %s", ctime(&now));
        }

        // Sleep the program so that i run every second
        sleep(1);
    }

    fclose(fp);

    return EXIT_SUCCESS;
}     
```
- This function sets up a daemon process that runs a certain program at a specific time specified as command line arguments.
- First, it checks if the user provided exactly 4 arguments, which are `hour, minute, second,` and `the path` of the program to run.
- Next, it checks whether the time fields in the arguments are valid. if the hour/minute/second is `""`, it outomatically sets the variable to **-1**, indicating that the program should run at any hour/minute/second. 
- If the fields are not `"*"`, then they must contain an integer value between **0-23** for hour, and **0-59** for the minute and second. If the fields are invalid, the program prints an error message and exits with a failure status.
- The `umask()` function is called to set the file mode creation mask to 0, which allows the daemon process to create files with any permissions.
- The `setsid()` function is called to create a new session and set the process group ID to the process ID of the calling process.
- A file pointer `fp` is opened in append mode for the log file, and the daemon process enters a while loop that runs indefinitely.
- In each iteration of the while loop, the current time is obtained using the `time()` function, and the local time is stored in a `tm` stucture using the `localtime_r()` function.
- The program checks if the current time matches the specified time fields. If the fields match, the function **call_path()** is called with the program path as an argument to run the program.
- The daemon process sleeps for 1 second using the `sleep()` function, and the loop repeats.
